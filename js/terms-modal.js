var terms = $('.terms'),
  termsOpen = $('.terms-open'),
  termsClose = $('.terms__close');

termsOpen.on('click', function() {
  terms.addClass('active');
});

termsClose.on('click', function() {
  terms.removeClass('active');
})