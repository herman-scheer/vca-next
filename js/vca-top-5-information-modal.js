var infoModal = $('.top-5-information-modal'),
    infoModalOpen = $('.page-title-information-icon'),
    infoModalClose = $('.top-5-information-modal__close')
    infoModalOverlay = $('.top-5-information-modal__overlay');

infoModalOpen.on('click', function() {
  infoModal.addClass('open');
});

infoModalClose.on('click', function() {
  infoModal.removeClass('open');
});

infoModalOverlay.on('click', function() {
  infoModal.removeClass('open');
});