// top 5 modal
var top5ModalTrigger = $('.top-5-slide__modal-trigger'),
    top5Modal = $('.top-5-slide__slick-arrow'),
    top5Close = $('.top-5-slide__close');

top5ModalTrigger.on('click', function () {
  top5Modal.addClass('open');
})

top5Close.on('click', function () {
  top5Modal.removeClass('open');
})


// top 11 finalists
var top11ModalTrigger = $('.top-11-slide__modal-trigger'),
    top11Modal = $('.top-11-slide__slick-arrow'),
    top11Close = $('.top-5-slide__close');

top11ModalTrigger.on('click', function () {
  top11Modal.addClass('open');
})

top11Close.on('click', function () {
  top11Modal.removeClass('open');
})