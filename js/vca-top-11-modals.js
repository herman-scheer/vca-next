// Top 11 Finalist Information
var finalistModalTrigger = $('.top-11-heading .icon-plus, .top-11-sub-heading .icon-plus')
var finalistModalClose = $('.top-11-info-modal__close')
var finalistModal = $('.top-11-info-modal')

finalistModalTrigger.on('click', function () {
  finalistModal.addClass('open')
})

finalistModalClose.on('click', function () {
  finalistModal.removeClass('open')
})

// Competition Summary
var compModalTrigger = $('.top-11-slide__about-comp');
var compModalClose = $('.comp-summary__close');
var compModal = $('.comp-summary');

compModalTrigger.on('click', function () {
  compModal.addClass('open')
})

compModalClose.on('click', function () {
  compModal.removeClass('open')
})


