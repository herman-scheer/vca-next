////////////////////////////////////
// Mobile Smooth Scroll
////////////////////////////////////
$('.nav-blocks a').click(function(e){
  e.preventDefault()
  var href = $(this).attr('href')  
  
  if (size == 'small' && href == '#ideas') {
    $('html, body').animate({
        scrollTop: ideas - 215
    }, 700); 
  } else if (size == 'small' || size == 'medium') {
    if(href == '#contest') {
      $('html, body').animate({
          scrollTop: contest
      }, 700);
    } else if (href == "#top-11") {
      $('html, body').animate({
          scrollTop: topeleven - 176
      }, 700); 
    } else if (href == "#ideas") {
      $('html, body').animate({
          scrollTop: ideas - 195
      }, 700); 
    } else if (href == "#top-5") {
      $('html, body').animate({
          scrollTop: topfive - 218
      }, 700); 
    } else {
      $('html, body').animate({
          scrollTop: $("#questions").offset().top - 170
      }, 700);
    }   
  }
})

$('.form-resources-mobile').click(function() {
  // e.preventDefault()
  var link = $(this).attr('href');

  if(size == 'small') {
    $('html, body').animate({
      scrollTop: ideas - 215
    }, 700);
  }
})

////////////////////////////////////
// Scroll Down
////////////////////////////////////
$('body').on('click', '#scroll-down', function(e) {
  e.preventDefault()
  if (size == 'large') {
    $.fn.fullpage.moveSlideRight();   
  } else {
    $('html, body').animate({
        scrollTop: contest
    }, 700)
  }
})

////////////////////////////////////
// To Top Button
////////////////////////////////////
$("#to-top").click(function(e) {
  e.preventDefault()
  $('html, body').animate({
      scrollTop: 0
  }, 1000)
  $(this).removeClass('fixed')
})