////////////////////////////////////
// Initiates Slick Slider
////////////////////////////////////
$(document).ready(function(){
  $('.gallery-slideshow').slick({
    rows: 2,
    infinite: true,
    mobileFirst: true,
    fade: true,
    cssEase: 'linear',
    speed: 300,
    rows: 2,
    slidesPerRow: 2,
    slidesToScroll: 1,
    slidesToShow: 1,
    responsive: [
      {
        breakpoint: 600,
        settings: {
          slidesPerRow: 3,
          slidesToScroll: 1,
          slidesToShow: 1
        }
      },
      {
        breakpoint: 1000,
        settings: {
          rows: 2,
          slidesPerRow: 4,
          slidesToScroll: 1,
          slidesToShow: 1
        }
      }
    ]
  });


  /////////////////////////
  // top 5 slider
  ////////////////////////

  // slider nav
  $('.top-5-slide__slick-nav').slick({
    slidesToShow: 5,
    asNavFor: '.top-5-slide__slick',
    focusOnSelect: true
  })

  // slider in modal
  $('.top-5-slide__slick').slick({
    slidesToShow: 1,
    asNavFor: '.top-5-slide__slick-nav',
    mobileFirst: true,
    responsive: [{

      breakpoint: 1000,
      settings: {
        appendArrows: $('.top-5-slide__slick-arrow')
      }

    }]
  })


  /////////////////////////
  // top 11 slider
  ////////////////////////

  // slider nav
  $('.top-11-slide__slick-nav').slick({
    slidesToShow: 12,
    asNavFor: '.top-11-slide__slick',
    focusOnSelect: true
  })

  // slider in modal
  $('.top-11-slide__slick').slick({
    slidesToShow: 1,
    asNavFor: '.top-11-slide__slick-nav',
    mobileFirst: true,
    responsive: [{

      breakpoint: 1000,
      settings: {
        appendArrows: $('.top-11-slide__slick-arrow')
      }

    }]
  })
})
