$(document).ready(function(){
  
  // Toggles Gallery Overlays
  $('body').on('click', '.gallery-image', function(){
    if (window.innerWidth > 640) {
      $(this).children().toggleClass('active');
    } else {
      $('.gallery-mobile-content').empty()
      dup = $(this).children().clone()
      $('.gallery-mobile-content').append(dup).addClass('active')
    }
    // if ( $("#ideas").css('padding-bottom') == '61px'  ) {
      // $('.gallery-mobile-content').empty()
      // dup = $(this).children().clone()
      // $('.gallery-mobile-content').append(dup).addClass('active')
    // } else {
      // $(this).children().toggleClass('active');
    // }
  })

  // Close mini mobile overlay
  $('.gallery-mobile-content').click(function() {
    $(this).removeClass('active')
  })

  // Toggles Q&A
  $('.question-title').click(function() {
    $(this).parent().toggleClass('active')
    $(this).next('p').slideToggle()
  })

  // Form Model Toggle
  $('#submit-idea').click(function(e){
    e.preventDefault()
    $('body, .form-modal').addClass('form-active')
  })

  // Close Form
  $('.close-modal, .form-resources, .form-resources-mobile').click(function(e){
    e.preventDefault()
    $('body, .form-modal').removeClass('form-active')
  })

  // Open Video Model and start play
  $('.watch-video').click(function(e){
    e.preventDefault()
    $('body, .video-modal').addClass('video-active')
    $('#modal-video').get(0).play()
  })

  // Close Video Modal
  $('.video-modal').click(function(e){
    e.preventDefault()
    $('body, .video-modal').removeClass('video-active')
  })

  // Back Home
  $('#back-home').click(function(e){
    e.preventDefault()
    $('.form-active').removeClass('form-active')
  })

  /////////////////////////////////////
  // Fake File Upload Functions
  /////////////////////////////////////
  // Trigger real browse button
  $('.file-browse, .file-path').click(function() {
    $('#submit-image').click();
  });

  // Update File Chosen
  $("#submit-image").change(function(){
    var file = $(this).val();
    var fileName = file.split("\\");
    $('.file-path').text((fileName[fileName.length-1]))
  });
})