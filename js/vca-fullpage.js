////////////////////////////////////
// Used for Transitions
////////////////////////////////////
// These run as you leave the previous slide
function introTransitions() {
  $('.master-footer').removeClass('fixed');
}

function otherTransitions() {
  $('.top-bar-bg').addClass('active')
}

// These run once the slide is complete

function introAfterTransitions(slideIndex) {
  $('.top-bar-bg').removeClass('active')
}

function contestAfterTransitions(slideIndex) {
  $('.master-footer').addClass('fixed');
  $('#magic-line, .contest-heading, .contest-slide-right, .contest-slide-left').addClass('full-active')
}

function top11AfterTransitions(slideIndex) {
  $('.master-footer').addClass('fixed');
  $('#magic-line4, .top-11-heading, .top-11').addClass('full-active')
}

function ideasAfterTransitions(slideIndex) {
  $('.master-footer').addClass('fixed');
  $('#magic-line2, .ideas-heading, .gallery-slideshow').addClass('full-active')
}

function top5AfterTransitions(slideIndex) {
  $('.master-footer').addClass('fixed');
  $('#magic-line3, .top-5-heading, .top-5').addClass('full-active')
}

function questionsAfterTransitions(slideIndex) {
  $('.master-footer').addClass('fixed');
  $('#magic-line5, .questions-heading, .questions').addClass('full-active')
}

////////////////////////////////////
// Initiates Fullpage.js
////////////////////////////////////
$('#fullpage').fullpage({
  controlArrows: false,
  touchSensitivity: 25,
  normalScrollElements: '.contest-slide-left, .contest-slide-right',
  onSlideLeave: function( anchorLink, index, slideIndex, direction, nextSlideIndex){
    if(nextSlideIndex === 0) {
      introTransitions()
    } else {
      otherTransitions()
    }
  },
  afterSlideLoad: function (anchorLink, index, slideAnchor, slideIndex){
    if (slideIndex === 1) {
      contestAfterTransitions(slideIndex)
    } else if (slideIndex === 2) {
      top11AfterTransitions(slideIndex)
    } else if (slideIndex === 3) {
      ideasAfterTransitions(slideIndex)
    } else if (slideIndex === 4) {
      top5AfterTransitions(slideIndex)
    } else if (slideIndex === 5) {
      questionsAfterTransitions(slideIndex)
    } else {
      introAfterTransitions(slideIndex)
    }

    // Disable scroll looping on first and last slides
    if (slideIndex === 0) {
      console.log('index 0!')
      $.fn.fullpage.setKeyboardScrolling(false, 'up, left');
    } else {
      $.fn.fullpage.setKeyboardScrolling(true, 'up, left');
    }
    if (slideIndex === 5) {
      console.log('index 5!')
      $.fn.fullpage.setKeyboardScrolling(false, 'down, right');
    } else {
      $.fn.fullpage.setKeyboardScrolling(true, 'down, right');
    }
  }
});

////////////////////////////////////
// Horizontal Scroll
////////////////////////////////////
// Mouse wheel event
var mousewheelevt = (/Firefox/i.test(navigator.userAgent)) ? "DOMMouseScroll" : "mousewheel" //FF doesn't recognize mousewheel as of FF3.x

// Keeps track of last scroll time
var timeStamp = new Date().getTime();

// On mousewheel
$('#fullpage').bind(mousewheelevt, function(e){
    var evt = window.event || e //equalize event object     
    evt = evt.originalEvent ? evt.originalEvent : evt; //convert to originalEvent if possible               

  // stores time of scroll
  var timeNow = new Date().getTime();

  // first and last slides when active
  var firstSlide = $('.fp-slidesContainer .slide:nth-child(1)').hasClass('active');
  var lastSlide = $('.fp-slidesContainer .slide:nth-child(6)').hasClass('active');

  // If last scroll was within 1.3 seconds
  // it doesn't scroll
  if (timeNow - timeStamp < 1400) {
    return;
  // Other wise....
  } else {
    // Gets wheel Y movement
    wheel = evt.detail ? evt.detail*(-40) : evt.wheelDelta //check for detail first, because it is used by Opera and FF

    // Updates timestamp
    timeStamp = timeNow;
    // 0 = tap and slide goes crazy
    if (wheel > 0 && !firstSlide) {
      $.fn.fullpage.moveSlideLeft();  
    } else if (wheel < 0 && !lastSlide) {
      $.fn.fullpage.moveSlideRight();   
    }
  }
});

////////////////////////////////////////
// Mobile Full Switch
////////////////////////////////////////
$(document).ready(function() {
  disableFullpage();
  $(window).resize(disableFullpage);
});

function disableFullpage(){
  if ($("#mobile").css("display") == "block" ){
    $('html').removeClass('fp-enabled')
    $('body').removeAttr('class')
    $('body, html').removeAttr('style')
  } else {
    $('.top-blue, .top-bar').removeClass('fixed')
  }
}

////////////////////////////////////
// overflow scroll on questions
////////////////////////////////////
$('.questions').bind('mousewheel', function(e) {
  e.stopPropagation()
})

////////////////////////////////////
// Navigation
////////////////////////////////////
$('.intro-link').click(function() {
 if (size == 'large') {
    $.fn.fullpage.moveTo(1, 0); 
 }
})

$('.contest-link').click(function() {
 if (size == 'large') {
    $.fn.fullpage.moveTo(1, 1); 
 }
})

$('.top-eleven-link').click(function() {
 if (size == 'large') {
  $.fn.fullpage.moveTo(1, 2);
 }
})

$('.ideas-link').click(function() {
 if (size == 'large') {
  $.fn.fullpage.moveTo(1, 3);
 }
})

$('.form-resources').click(function() {
 if (size == 'large') {
  $.fn.fullpage.moveTo(1, 3);
 }
})

$('.top-five-link').click(function() {
 if (size == 'large') {
  $.fn.fullpage.moveTo(1, 4);
 }
})

$('.questions-link').click(function() {
 if (size == 'large') {
    $.fn.fullpage.moveTo(1, 5);
 }  
})

$(window).ready(function(){
  $('.top-bar, h1, .intro-slide-right, .vca-next-transition, #scroll-down').addClass('full-active')
})