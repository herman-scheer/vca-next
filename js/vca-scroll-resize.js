///////////////////////////////////
// Global Variables
//////////////////////////////////
// Section Positions
contest = null
ideas = null
questions = null
// Breakpoint size
size = null
// Current Scroll Position
pagePosition = $(window).scrollTop()
windowHeight = $(window).height()

///////////////////////////////////
// Sets breakpoint size
//////////////////////////////////
function checkSize() {
  if ($(".size-check").css('z-index') == '-4' ) {
    size = 'large'
  } else if ($(".size-check").css('z-index') == '-3') {
    size = 'medium-large'
  } else if ($(".size-check").css('z-index') == '-2') {
    size = 'medium'
  } else {
    size = 'small'
  }
}

///////////////////////////////////
// Sets Section Positions
//////////////////////////////////
function setPositions() {
  if(size !== 'large') {
    contest = $('#contest').offset().top
    ideas = $('#ideas').offset().top
    topfive = $('#top-5').offset().top
    topeleven = $('#top-11').offset().top
    questions =$('#questions').offset().top
  }
}

///////////////////////////////////
// Toggles Sticky Stuff
//////////////////////////////////
function sticky() {
  if (size !== 'large') {
    if ( pagePosition >= $('#contest').offset().top ) {
      $('.top-bar, .top-blue').addClass('fixed')
    } else {
      $('.top-bar, .top-blue').removeClass('fixed')
    }
  }
}

///////////////////////////////////
// Displays to top at bottom
//////////////////////////////////
function toTopBottom() {
  if (size !== 'large') {
    if (pagePosition + windowHeight == $(document).height()) {
      $('#to-top').addClass('fixed')
    }
  }
}

///////////////////////////////////
// Changes Heading and Nav Block
//////////////////////////////////
function activePage() {
  if (size !== 'large') {

    // questions
    if ( size == 'small' && pagePosition >= (questions - 240) || pagePosition == ($(document).height() - $(window).height())   ) {
      $('.nav-blocks a, .mobile-sub-heading').removeClass('active')
      $('#questions-block, #questions-heading').addClass('active')
    } 
    else if ( size == 'medium' && pagePosition >= (questions - 193)  ) {
      $('.nav-blocks a, .mobile-sub-heading').removeClass('active')
      $('#questions-block, #questions-heading').addClass('active')
    } 
    else if ( size == 'medium-large' && pagePosition >= (questions - 230)  ) {
      $('.nav-blocks a, .mobile-sub-heading').removeClass('active')
      $('#questions-block, #questions-heading').addClass('active')
    } 

    // top 5
    else if (size == 'small' && pagePosition > (topfive - 295)) {
      $('.nav-blocks a, .mobile-sub-heading').removeClass('active')
      $('#top-5-block, #top-5-heading').addClass('active')
    } 
    else if (size == 'medium' && pagePosition > (topfive - 175)) {
      $('.nav-blocks a, .mobile-sub-heading').removeClass('active')
      $('#top-5-block, #top-5-heading').addClass('active')
    }
    else if (size == 'medium-large' && pagePosition > (topfive - 217)) {
      $('.nav-blocks a, .mobile-sub-heading').removeClass('active')
      $('#top-5-block, #top-5-heading').addClass('active')
    }

    // ideas
    else if (size == 'small' && pagePosition > (ideas - 216)) {
      $('.nav-blocks a, .mobile-sub-heading').removeClass('active')
      $('#ideas-block, #ideas-heading').addClass('active')
    } 
    else if (size == 'medium' && pagePosition > (ideas - 175)) {
      $('.nav-blocks a, .mobile-sub-heading').removeClass('active')
      $('#ideas-block, #ideas-heading').addClass('active')
    }
    else if (size == 'medium-large' && pagePosition > (ideas - 217)) {
      $('.nav-blocks a, .mobile-sub-heading').removeClass('active')
      $('#ideas-block, #ideas-heading').addClass('active')
    }

    // top 11
    else if (size == 'small' && pagePosition > (topeleven - 395)) {
      $('.nav-blocks a, .mobile-sub-heading').removeClass('active')
      $('#top-11-block, #top-11-heading').addClass('active')
    } 
    else if (size == 'medium' && pagePosition > (topeleven - 175)) {
      $('.nav-blocks a, .mobile-sub-heading').removeClass('active')
      $('#top-11-block, #top-11-heading').addClass('active')
    }
    else if (size == 'medium-large' && pagePosition > (topeleven - 217)) {
      $('.nav-blocks a, .mobile-sub-heading').removeClass('active')
      $('#top-11-block, #top-11-heading').addClass('active')
    }
    
    else {
      $('.nav-blocks a, .mobile-sub-heading').removeClass('active')
      $('#contest-block, #contest-heading').addClass('active')     
    }
  }
}

///////////////////////////////////
// On Mouse Wheel
//////////////////////////////////
$('#mobile').bind(mousewheelevt, function(e){
    var evt = window.event || e //equalize event object     
    evt = evt.originalEvent ? evt.originalEvent : evt; //convert to originalEvent if possible               
    wheel = evt.detail ? evt.detail*(-40) : evt.wheelDelta
  if (wheel > 0) {
    $('#to-top').addClass('fixed') 
  } else if (wheel < 0 || $(window).scrollTop() == 0) {
    $('#to-top').removeClass('fixed')  
  }
});

var ts;
$('#mobile').bind('touchstart', function (e){
   ts = e.originalEvent.touches[0].clientY;
});

$(document).bind('touchend', function (e){
   var te = e.originalEvent.changedTouches[0].clientY;
   if(ts > te+5){
      $('#to-top').removeClass('fixed') 
   }else if(ts < te-5){
      $('#to-top').addClass('fixed') 
   }
});
///////////////////////////////////
// On Resize
//////////////////////////////////
$(window).resize(function(){
  pagePosition = $(window).scrollTop();
  windowHeight = $(window).height()
  checkSize()
  setPositions()
  toTopBottom()
})

///////////////////////////////////
// On scroll
//////////////////////////////////
$(window).scroll(function(){
  pagePosition = $(window).scrollTop()
  sticky()
  activePage()
  toTopBottom()
})

///////////////////////////////////
// On Page Load
//////////////////////////////////
$(window).ready(function() {
  checkSize()
  setPositions()
  sticky()
})