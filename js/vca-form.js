// function validateRange(ColumnId, RangeType) {
//   if(document.getElementById('rangeUsedMsg'+ColumnId)) {
//     var field = document.getElementById('Field'+ColumnId);
//     var msg = document.getElementById('rangeUsedMsg'+ColumnId);

//     switch(RangeType) {
//       case 'character':
//         msg.innerHTML = field.value.length;
//         break;
        
//       case 'word':
//         var val = field.value;
//         val = val.replace(/\n/g, " ");
//         var words = val.split(" ");
//         var used = 0;
//         for(i =0; i < words.length; i++) {
//           if(words[i].replace(/\s+$/,"") != "") used++;
//         }
//         msg.innerHTML = used;
//         break;
        
//       case 'digit':
//         msg.innerHTML = field.value.length;
//         break;
//     }
//   }
// }

var submitted = false;

function formValidate() {
  var checkboxChecked = document.getElementById('Field107').checked;
  var errorMessage = document.getElementById('terms-error-message');
  var wordCount = document.getElementById('rangeUsedMsg6').innerHTML;
  var descriptionMessage = document.getElementById('description-error-message');
  var inputField = document.getElementsByClassName('input-field');
  var inputError = document.getElementById('error-message');

  var f1 = document.forms['form1'].Field1.value;
  var f2 = document.forms['form1'].Field212.value;
  var f3 = document.forms['form1'].Field215.value;
  var f4 = document.forms['form1'].Field4.value;
  var f5 = document.forms['form1'].Field210.value;
  var f6 = document.forms['form1'].Field5.value;
  var f7 = document.forms['form1'].Field6.value;

  // console.log(inputField.value);

  function inputFields() {
    if (f1=='' || f2=='' || f3=='' || f4=='' || f5=='' || f6=='' || f7=='') {
      inputError.className = 'error';
    } else {
      inputError.className = '';
      return true;
    }
  }

  function terms() {
    if (!checkboxChecked) {
      errorMessage.className = 'error';
    } else {
      errorMessage.className = '';
      return true;
    }
  }

  function description() {
    if (wordCount > 250) {
      descriptionMessage.className = 'error';
    } else {
      descriptionMessage.className = ' ';
      return true;
    }
  }

  // inputFields();
  terms();
  // description();

  // if (inputFields() && terms() && description()) {
  if (terms()) {
    submitted = true;
  } else {
    return false
  }

}