$(document).ready(function(){
  // Initiate Countdown
  $('#count-down').countdown('2016/05/27 18:00:00', function(event) {
    $(this).html(event.strftime('<span class="green">%D</span> <span class="both">Days</span> <span class="green">%H</span> <span class="full">Hr</span> <span class="mobile">:</span> <span class="green">%M</span> <span class="mobile">:</span><span class="full">Min</span> <span class="green">%S</span> <span class="full">Sec</span>'));
  })  
})