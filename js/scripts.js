// Dependencies
@import "./jquery.js";
@import "./jquery-mobile.js";
@import "./countdown.js";
@import "./slick.js";
@import "./fullpage.js";

// Scripts
@import "./vca-fullpage.js";
@import "./vca-slick.js";
@import "./vca-scroll-resize.js";
@import "./vca-countdown.js";
@import "./vca-toggles.js";
@import "./vca-smooth-scrolls.js";
@import "./vca-magicline.js";
@import "./vca-transitions.js";
@import "./terms-modal.js";
// @import "./vca-top-5-char-limit.js";
@import "./vca-top-5-modal.js";
@import "./vca-top-5-information-modal.js";
@import "./vca-top-11-modals.js"
