# VCA Next

## Dev Dependencies
- [NPM](https://docs.npmjs.com/getting-started/installing-node)
- [Sass](http://sass-lang.com/install)
- [Grunt Js](http://gruntjs.com/getting-started)

## Installation
Before installing, please make sure you meet all the dependencies listed above.

### Step 1: Clone This Repo
Start by cloning this repo to your local computer.

```
git clone git@bitbucket.org:herman-scheer/vca-next.git
```

### Step 2: Install NPM Modules
CD into the created directory and install all our development dependencies with the following commands:
```
cd VCA-Next
npm install
```

### Step 3: Build the site
Create a production ready build of the website (it will be located at '/build').
```
grunt build
```

### Step 4: Start the web server and watch files
Start the webserver ([localhost:3000](http://localhost:3000)) and watch all files for changes with the following command:
```
grunt live
```

## Grunt Commands

### Production Build / Watch Files / Server
```
grunt build && grunt live
```

### Production Build
```
grunt build
```

### Watch Files / Start Server
```
grunt live
```

## Around the code

### Breaking it up
All HTML/CSS/JS is broken into managable bites and combined through grunt tasks.

- All sass is imported in `/css/style.scss`
- All JS is imported in `/js/scripts.js`
- HTML is broken into partials (`/partials`) and comes together though `index.html`