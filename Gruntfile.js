module.exports = function(grunt) {
  // Config
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    import: {
      dist: {
        files: {
          './build/js/scripts.js': './js/scripts.js',
          './build/js/scripts-wufoo.js' : './js/scripts-wufoo.js'
        }
      }
    },
    uglify: {
      build: {
        files: {
          './build/js/scripts.min.js': './build/js/scripts.js',
          './build/js/scripts-wufoo.min.js': './build/js/scripts-wufoo.js'
        }
      }
    },
    sass: {
      options: {
        sourcemap: 'none',
        style: 'compressed'
      },
      dist: {
        files: {
          'build/css/style.css': 'css/style.scss'
        }
      }
    },
    copy: {
      main: {
        files: [
          {expand: true, src: ['./videos/*.{mp4,webm,ogv}'], dest: 'build/'},
          {expand: true, src: ['images/*.{png,jpg,gif,svg}'], dest: 'build/'},
          {expand: true, src: ['images/*.{png,jpg,gif,svg}'], dest: 'build/next/'}
        ]
      }
    },
    includes: {
      build: {
        cwd: './',
        src: [ '*.html'],
        dest: 'build/',
        options: {
          flatten: true,
          includePath: ['partials', 'partials/svg'],
        }
      }
    },
    watch: {
      html: {
        files: [ './*.html', './partials/*.html', './partials/svg' ],
        tasks: ['includes']
      },
      sass: {
        files: [ './css/*.scss' ],
        tasks: ['sass']
      },
      js: {
        files: ['./js/*.js'],
        tasks: ['import', 'uglify']
      },
      copy: {
        files: ['./videos/*.{mp4,webm,ogg}', 'images/*.{png,jpg,gif,svg}'],
        tasks: ['copy']
      },
      options: {
        livereload: true
      }
    },
    connect: {
      server: {
        options: {
          livereload: true,
          base: 'build/',
          port: 3000
        }
      }
    }
  });

  // Load Plugins
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-import');
  grunt.loadNpmTasks('grunt-includes');

  // Tasks
  grunt.registerTask('build', ['includes', 'sass', 'import', 'uglify', 'copy']);
  grunt.registerTask('live', ['connect', 'watch']);
};